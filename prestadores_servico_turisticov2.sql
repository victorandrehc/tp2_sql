-- MySQL dump 10.11
--
-- Host: localhost    Database: prestadores_servicos_turisticos
-- ------------------------------------------------------
-- Server version	5.0.77
DROP DATABASE IF EXISTS prestadores_servicos_turisticos;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE DATABASE IF NOT EXISTS prestadores_servicos_turisticos;

USE prestadores_servicos_turisticos;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `EMPRESA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `EMPRESA` (
  `cnpj` varchar(18) NOT NULL,
  `nome` varchar(69) NOT NULL,
  `situacao` varchar(14) NOT NULL,
  `capacidade` int(4),
  `cod_certif` varchar(19) NOT NULL,
  PRIMARY KEY  (`cnpj`),
  FOREIGN KEY (`cod_certif`) REFERENCES `CERTIFICADO`(`cod_certif`) ON DELETE RESTRICT,
  UNIQUE (`nome`,`cod_certif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `autor`
--

DROP TABLE IF EXISTS `LOCAL`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `LOCAL` (
  `cnpj` varchar(18) NOT NULL,
  `cep` int(8) NOT NULL,
  `uf` char(2) NOT NULL,
  `cidade` varchar(40) NOT NULL,
  `bairro`varchar(48) NOT NULL,
  `logradouro`varchar(89) NOT NULL,
  PRIMARY KEY  (`cnpj`),
  FOREIGN KEY(`cnpj`) REFERENCES `EMPRESA`(`cnpj`) ON DELETE CASCADE,
  UNIQUE (`logradouro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
--
-- Table structure for table `certificado`
--

DROP TABLE IF EXISTS `CERTIFICADO`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `CERTIFICADO` (
  `cod_certif` varchar(19) NOT NULL,
  `nat_jur` varchar(34) NOT NULL,
  `atividade` varchar(11) NOT NULL,
  `porte` varchar(32) NOT NULL,
  `data_ger` varchar(10) NOT NULL,
  `data_val` varchar(10) NOT NULL,
  PRIMARY KEY  (`cod_certif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `cnae`
--

DROP TABLE IF EXISTS `CNAE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `CNAE` (
  `id_cnae` int(2) NOT NULL,
  `codigo` varchar(10)  NOT NULL,
  `descricao` varchar(117) NOT NULL,
  PRIMARY KEY  (`id_cnae`),
  UNIQUE(`codigo`,`descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `edicao`
--

DROP TABLE IF EXISTS `LINGUA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `LINGUA` (
  `id_lingua` int(2) NOT NULL,
  `nome` varchar(8) NOT NULL,
  PRIMARY KEY  (`id_lingua`),
  UNIQUE(`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `empresa_lingua`
--

DROP TABLE IF EXISTS `EMPRESA_LINGUA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `EMPRESA_LINGUA` (
  `cnpj` varchar(18) NOT NULL,
  `id_lingua` int(2) NOT NULL,
  PRIMARY KEY  (`cnpj`, `id_lingua`),
  FOREIGN KEY (`cnpj`) REFERENCES `EMPRESA`(`cnpj`) ON DELETE CASCADE,
  FOREIGN KEY (`id_lingua`) REFERENCES `LINGUA`(`id_lingua`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `empresa_telefone`
--

DROP TABLE IF EXISTS `EMPRESA_TELEFONE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `EMPRESA_TELEFONE` (
  `telefone` varchar(15) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  PRIMARY KEY  (`telefone`),
  FOREIGN KEY(`cnpj`) REFERENCES `EMPRESA`(`cnpj`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `empresa_email`
--

DROP TABLE IF EXISTS `EMPRESA_EMAIL`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `EMPRESA_EMAIL` (
  `email` varchar(41) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  PRIMARY KEY  (`email`),
  FOREIGN KEY(`cnpj`) REFERENCES `EMPRESA`(`cnpj`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `certificado_cnae`
--

DROP TABLE IF EXISTS `CERTIFICADO_CNAE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `CERTIFICADO_CNAE` (
  `cod_certif` varchar(19) NOT NULL,
  `id_cnae` int(2) NOT NULL,
  PRIMARY KEY  (`cod_certif`,`id_cnae`),
  FOREIGN KEY (`cod_certif`) REFERENCES `CERTIFICADO`(`cod_certif`) ON DELETE CASCADE,
  FOREIGN KEY (`id_cnae`) REFERENCES `CNAE`(`id_cnae`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;