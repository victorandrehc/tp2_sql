# -*- coding: latin1-*-
import sys
from PyQt4 import QtCore,QtGui
from db_manager import DatabaseManager
from models.empresa import Empresa
from models.certificado import Certificado
from models.cnae import Cnae
from models.lingua import Lingua
from models.local import Local
from models.certificado_cnae import CertificadoCnae
from models.email import Email
from models.telefone import Telefone
from models.empresa_lingua import EmpresaLingua

from models.resultados import ResultadoPesquisaBasica,ResultadoPesquisaAvancada

#Ao trocar a UI deve se rodar o comando
#pyuic4 editorFrame.ui -o editorFr'ame.py

from editorFrame import Ui_TP2

class Editor(QtGui.QMainWindow):

    def __init__(self):
        super(Editor, self).__init__()
        self.ui=Ui_TP2()
        self.ui.setupUi(self)
        self.db_manager=DatabaseManager()

        #sinais com a ui
        self.ui.chbDuplicatas.setChecked(True)
        self.ui.chbDuplicatasAvancado.setChecked(True)

        self.campos={'cnpj':'cnpj',# str
                    'razao social':'razao_social', #str
                    'nome fantasia':'nome_fant', #str
                    'situacao':'situacao',  #str
                    'capacidade':'capacidade', #inteiro
                    'cidade':'cidade', #str
                    'estado sigla':'uf', #str
                    'logradouro':'logradouro', #str
                    'codigo do certificado':'cod_certif', #str
                    'codigo CNAE':'codigo'}#str

        self.operadores=['=','!=','>','<','>=','<=']


        #campos de pesquisa basica

        self.ui.cbFiltro.addItems(self.campos.keys())
        self.ui.cbOperador.addItems(self.operadores)

        self.ui.btPesquisar.clicked.connect(self.pesquisar)

        #campos de pesquisa avançada

        self.agredadores={'media':'AVG','Maximo':'MAX','Minimo':'MIN','Quantidade':'COUNT'}
        self.parametros={'capacidades':'capacidade',"cnpj's":'cnpj','linguas':'id_lingua'}
        self.grupos={'cidades':'cidade','estados':'uf','situacoes':'situacao',"cnpj's":'cnpj'}

        self.ui.cbFiltroAvancado.addItems(self.campos.keys())
        self.ui.cbOperadorAvancado.addItems(self.operadores)
        self.ui.cbAgregador.addItems(self.agredadores.keys())
        self.ui.cbParametro.addItems(self.parametros.keys())
        self.ui.cbGrupos.addItems(self.grupos.keys())

        self.ui.btPesquisarAvancado.clicked.connect(self.pesquisar_avancado)



        self.connect(self.ui.twResultado.horizontalHeader(),
                     QtCore.SIGNAL('sectionClicked (int)'),
                     self.header_clicked)

        self.show()

    def realiza_consulta(self,consulta):

         self.ui.lbStatus.setText('Consultando')
         resultado=self.db_manager.execute(consulta)
         print "Consulta realizada em {}ms".format(resultado['elapsed'])
         if resultado['tuples']!=None and resultado['tuples']!=[]:
            tuplas=[ResultadoPesquisaBasica(i) for i in resultado['tuples']]
            #print [i.cnpj for i in tuplas]
            self.ui.twResultado.setColumnCount(len(tuplas[0].HEADER))
            self.ui.twResultado.setRowCount(len(tuplas))
            self.ui.twResultado.setHorizontalHeaderLabels(tuplas[0].HEADER)
            for tupla,i in zip(tuplas,range(len(tuplas))):
                for atributo,j in zip(tupla.get_resultado(),range(len(tupla.get_resultado()))):
                    if atributo==None:
                        atributo=''
                    item=QtGui.QTableWidgetItem(atributo)
                    self.ui.twResultado.setItem(i,j,item)
            self.ui.lbStatus.setText('Esperando uma consulta')


    def realiza_consulta_avancada(self,consulta):

        self.ui.lbStatus.setText('Consultando')
        resultado=self.db_manager.execute(consulta)
        print "Consulta realizada em {}ms".format(resultado['elapsed'])
        if resultado['tuples']!=None and resultado['tuples']!=[]:
            tuplas=[ResultadoPesquisaAvancada(i) for i in resultado['tuples']]
            #print [i.cnpj for i in tuplas]
            self.ui.twResultado.setColumnCount(len(tuplas[0].HEADER))
            self.ui.twResultado.setRowCount(len(tuplas))
            self.ui.twResultado.setHorizontalHeaderLabels(tuplas[0].HEADER)
            for tupla,i in zip(tuplas,range(len(tuplas))):
                for atributo,j in zip(tupla.get_resultado(),range(len(tupla.get_resultado()))):
                    if atributo==None:
                        atributo=''
                    item=QtGui.QTableWidgetItem(atributo)
                    self.ui.twResultado.setItem(i,j,item)
        self.ui.lbStatus.setText('Esperando uma consulta')

    def pesquisar(self):
        self.monta_consulta()

    def pesquisar_avancado(self):
        self.monta_consulta_avancado()

     
    def header_clicked(self, i):
        print 'coluna {} selecionada'.format(i)
        extra="ORDER BY "
        if self.ui.tabwPesquisas.currentIndex()==0:
            extra+=self.campos[ResultadoPesquisaBasica().HEADER[i]]
            self.monta_consulta(extra)
        else:
            self.monta_consulta_avancado(i)

    def monta_consulta(self,extra=''):
        print 'realizando pesquisa'
        consulta="SELECT "
        if self.ui.chbDuplicatas.isChecked():
            consulta+="DISTINCT "
        consulta+="cnpj, razao_social, nome_fant, logradouro, cidade " #adicionando colunas desejadas
        consulta+="FROM EMPRESA NATURAL JOIN LOCAL "
        #consulta+="FROM EMPRESA NATURAL JOIN LOCAL NATURAL JOIN CERTIFICADO NATURAL JOIN CERTIFICADO_CNAE NATURAL JOIN CNAE "
        
        if not self.ui.chbNaoAplicarFiltro.isChecked():
            campo=self.campos.keys()[self.ui.cbFiltro.currentIndex()]
            if campo == 'codigo do certificado':
                consulta+= " NATURAL JOIN CERTIFICADO "
            elif campo=='codigo CNAE':
                consulta+= " NATURAL JOIN CERTIFICADO "
                consulta+= "NATURAL JOIN CERTIFICADO_CNAE NATURAL JOIN CNAE "

            consulta+="WHERE "
            consulta+=self.campos[campo]+' '+self.operadores[self.ui.cbOperador.currentIndex()]+' '
            if  campo !='capacidade': #se não for a capacidade é uma string
                #consulta+="'"+self.ui.leValor.text()+"'"
                consulta+="'"+unicode(self.ui.leValor.text())+"'"

            else:
                #consulta+=self.ui.leValor.text()
                consulta+=unicode(self.ui.leValor.text())
        consulta+=' '+extra
        consulta+=';'
        print consulta
        self.realiza_consulta(consulta)

    def monta_consulta_avancado(self,extra=False):
        print 'realizando pesquisa avançada'
        consulta="SELECT "
        if self.ui.chbDuplicatasAvancado.isChecked():
            consulta+="DISTINCT "
        consulta+=self.grupos[self.grupos.keys()[self.ui.cbGrupos.currentIndex()]]+","
        consulta+=self.agredadores[self.agredadores.keys()[self.ui.cbAgregador.currentIndex()]]+"("
        consulta+=self.parametros[self.parametros.keys()[self.ui.cbParametro.currentIndex()]]+") "
        consulta+="FROM EMPRESA NATURAL JOIN LOCAL NATURAL JOIN CERTIFICADO NATURAL JOIN CERTIFICADO_CNAE NATURAL JOIN CNAE NATURAL JOIN EMPRESA_LINGUA NATURAL JOIN LINGUA "

        if not self.ui.chbNaoAplicarFiltroAvancado.isChecked():
            campo=self.campos.keys()[self.ui.cbFiltroAvancado.currentIndex()]

            consulta+="WHERE "
            consulta+=self.campos[campo]+' '+self.operadores[self.ui.cbOperadorAvancado.currentIndex()]+' '
            if  campo !='capacidade': #se não for a capacidade é uma string
                #consulta+="'"+self.ui.leValor.text()+"'"
                consulta+="'"+unicode(self.ui.leValorAvancado.text())+"'"

            else:
                #consulta+=self.ui.leValor.text()
                consulta+=unicode(self.ui.leValorAvancado.text())
        consulta+=" GROUP BY "+self.grupos[self.grupos.keys()[self.ui.cbGrupos.currentIndex()]]
        #consulta+=' '+extra
        if extra!=False:
            campos=self.grupos[self.grupos.keys()[self.ui.cbGrupos.currentIndex()]]+","
            campos+=self.agredadores[self.agredadores.keys()[self.ui.cbAgregador.currentIndex()]]+"("
            campos+=self.parametros[self.parametros.keys()[self.ui.cbParametro.currentIndex()]]+") "
            campos=campos.split(',')
            consulta+=' ORDER BY '+campos[extra]
        consulta+=';'
        print consulta
        if self.parametros[self.parametros.keys()[self.ui.cbParametro.currentIndex()]]=='cnpj' and self.agredadores[self.agredadores.keys()[self.ui.cbAgregador.currentIndex()]]!="COUNT"  :
            print 'consulta SQL impossivel'
            self.ui.lbStatus.setText('consulta SQL impossivel')
            return
        else:
            self.realiza_consulta_avancada(consulta)

def main():
    app = QtGui.QApplication(sys.argv)
    ex = Editor()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()