# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'editorFrame.ui'
#
# Created: Sat Nov  5 16:13:27 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_TP2(object):
    def setupUi(self, TP2):
        TP2.setObjectName(_fromUtf8("TP2"))
        TP2.resize(800, 480)
        self.tabwPesquisas = QtGui.QTabWidget(TP2)
        self.tabwPesquisas.setGeometry(QtCore.QRect(15, 40, 771, 191))
        self.tabwPesquisas.setObjectName(_fromUtf8("tabwPesquisas"))
        self.simples = QtGui.QWidget()
        self.simples.setObjectName(_fromUtf8("simples"))
        self.label = QtGui.QLabel(self.simples)
        self.label.setGeometry(QtCore.QRect(10, 10, 421, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.cbFiltro = QtGui.QComboBox(self.simples)
        self.cbFiltro.setGeometry(QtCore.QRect(10, 50, 151, 31))
        self.cbFiltro.setObjectName(_fromUtf8("cbFiltro"))
        self.cbOperador = QtGui.QComboBox(self.simples)
        self.cbOperador.setGeometry(QtCore.QRect(180, 50, 151, 31))
        self.cbOperador.setObjectName(_fromUtf8("cbOperador"))
        self.leValor = QtGui.QLineEdit(self.simples)
        self.leValor.setGeometry(QtCore.QRect(350, 50, 161, 31))
        self.leValor.setObjectName(_fromUtf8("leValor"))
        self.btPesquisar = QtGui.QPushButton(self.simples)
        self.btPesquisar.setGeometry(QtCore.QRect(550, 100, 131, 29))
        self.btPesquisar.setObjectName(_fromUtf8("btPesquisar"))
        self.chbDuplicatas = QtGui.QCheckBox(self.simples)
        self.chbDuplicatas.setGeometry(QtCore.QRect(540, 10, 151, 26))
        self.chbDuplicatas.setObjectName(_fromUtf8("chbDuplicatas"))
        self.chbNaoAplicarFiltro = QtGui.QCheckBox(self.simples)
        self.chbNaoAplicarFiltro.setGeometry(QtCore.QRect(540, 40, 151, 26))
        self.chbNaoAplicarFiltro.setObjectName(_fromUtf8("chbNaoAplicarFiltro"))
        self.tabwPesquisas.addTab(self.simples, _fromUtf8(""))
        self.avacadas = QtGui.QWidget()
        self.avacadas.setObjectName(_fromUtf8("avacadas"))
        self.label_2 = QtGui.QLabel(self.avacadas)
        self.label_2.setGeometry(QtCore.QRect(0, 70, 421, 21))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.chbNaoAplicarFiltroAvancado = QtGui.QCheckBox(self.avacadas)
        self.chbNaoAplicarFiltroAvancado.setGeometry(QtCore.QRect(540, 70, 151, 26))
        self.chbNaoAplicarFiltroAvancado.setObjectName(_fromUtf8("chbNaoAplicarFiltroAvancado"))
        self.cbFiltroAvancado = QtGui.QComboBox(self.avacadas)
        self.cbFiltroAvancado.setGeometry(QtCore.QRect(0, 100, 151, 31))
        self.cbFiltroAvancado.setObjectName(_fromUtf8("cbFiltroAvancado"))
        self.chbDuplicatasAvancado = QtGui.QCheckBox(self.avacadas)
        self.chbDuplicatasAvancado.setGeometry(QtCore.QRect(340, 70, 151, 26))
        self.chbDuplicatasAvancado.setObjectName(_fromUtf8("chbDuplicatasAvancado"))
        self.cbOperadorAvancado = QtGui.QComboBox(self.avacadas)
        self.cbOperadorAvancado.setGeometry(QtCore.QRect(170, 100, 151, 31))
        self.cbOperadorAvancado.setObjectName(_fromUtf8("cbOperadorAvancado"))
        self.leValorAvancado = QtGui.QLineEdit(self.avacadas)
        self.leValorAvancado.setGeometry(QtCore.QRect(340, 100, 161, 31))
        self.leValorAvancado.setObjectName(_fromUtf8("leValorAvancado"))
        self.btPesquisarAvancado = QtGui.QPushButton(self.avacadas)
        self.btPesquisarAvancado.setGeometry(QtCore.QRect(540, 100, 131, 29))
        self.btPesquisarAvancado.setObjectName(_fromUtf8("btPesquisarAvancado"))
        self.cbAgregador = QtGui.QComboBox(self.avacadas)
        self.cbAgregador.setGeometry(QtCore.QRect(0, 10, 151, 31))
        self.cbAgregador.setObjectName(_fromUtf8("cbAgregador"))
        self.cbParametro = QtGui.QComboBox(self.avacadas)
        self.cbParametro.setGeometry(QtCore.QRect(200, 10, 151, 31))
        self.cbParametro.setObjectName(_fromUtf8("cbParametro"))
        self.label_3 = QtGui.QLabel(self.avacadas)
        self.label_3.setGeometry(QtCore.QRect(380, 10, 101, 21))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.avacadas)
        self.label_4.setGeometry(QtCore.QRect(160, 20, 31, 21))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.cbGrupos = QtGui.QComboBox(self.avacadas)
        self.cbGrupos.setGeometry(QtCore.QRect(480, 10, 151, 31))
        self.cbGrupos.setObjectName(_fromUtf8("cbGrupos"))
        self.tabwPesquisas.addTab(self.avacadas, _fromUtf8(""))
        self.twResultado = QtGui.QTableWidget(TP2)
        self.twResultado.setGeometry(QtCore.QRect(10, 240, 781, 231))
        self.twResultado.setObjectName(_fromUtf8("twResultado"))
        self.twResultado.setColumnCount(0)
        self.twResultado.setRowCount(0)
        self.lbStatus = QtGui.QLabel(TP2)
        self.lbStatus.setGeometry(QtCore.QRect(20, 10, 641, 21))
        self.lbStatus.setObjectName(_fromUtf8("lbStatus"))

        self.retranslateUi(TP2)
        self.tabwPesquisas.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(TP2)

    def retranslateUi(self, TP2):
        TP2.setWindowTitle(_translate("TP2", "TP2", None))
        self.label.setText(_translate("TP2", "Exibir Empresas que:", None))
        self.btPesquisar.setText(_translate("TP2", "Pesquisar", None))
        self.chbDuplicatas.setText(_translate("TP2", "Eliminar duplicatas", None))
        self.chbNaoAplicarFiltro.setText(_translate("TP2", "Não aplicar filtros", None))
        self.tabwPesquisas.setTabText(self.tabwPesquisas.indexOf(self.simples), _translate("TP2", "Pesquisas Simples", None))
        self.label_2.setText(_translate("TP2", "de uma Empresa que", None))
        self.chbNaoAplicarFiltroAvancado.setText(_translate("TP2", "Não aplicar filtros", None))
        self.chbDuplicatasAvancado.setText(_translate("TP2", "Eliminar duplicatas", None))
        self.btPesquisarAvancado.setText(_translate("TP2", "Pesquisar", None))
        self.label_3.setText(_translate("TP2", "Agrupar por", None))
        self.label_4.setText(_translate("TP2", "das", None))
        self.tabwPesquisas.setTabText(self.tabwPesquisas.indexOf(self.avacadas), _translate("TP2", "Pesquisas Avançadas", None))
        self.lbStatus.setText(_translate("TP2", "Esperando uma consulta", None))

