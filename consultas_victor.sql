
USE prestadores_servicos_turisticos;
SET profiling = 1;
/*3.1*/
SELECT SQL_NO_CACHE razao_social,nome_fant,logradouro FROM EMPRESA NATURAL JOIN LOCAL WHERE cidade='Belo Horizonte' ORDER BY capacidade;
/*3.1*/
SELECT SQL_NO_CACHE razao_social,nome_fant,logradouro FROM EMPRESA NATURAL JOIN \
	LOCAL WHERE cnpj IN (SELECT cnpj FROM LOCAL WHERE cidade='Belo Horizonte') ORDER BY capacidade;

/*6.1*/
SELECT SQL_NO_CACHE razao_social,nome_fant,logradouro,email,telefone FROM EMPRESA NATURAL JOIN LOCAL NATURAL JOIN EMPRESA_EMAIL NATURAL JOIN EMPRESA_TELEFONE \
	WHERE cidade='Belo Horizonte' ORDER BY capacidade;
/*6.2*/
SELECT SQL_NO_CACHE razao_social,nome_fant,logradouro,email,telefone FROM EMPRESA NATURAL JOIN LOCAL NATURAL JOIN EMPRESA_EMAIL NATURAL JOIN EMPRESA_TELEFONE \
	 WHERE cnpj IN (SELECT cnpj FROM LOCAL AS L WHERE cidade='Belo Horizonte') ORDER BY capacidade;


/*9.1*/
SELECT SQL_NO_CACHE cidade,AVG(capacidade) FROM EMPRESA NATURAL JOIN LOCAL GROUP BY cidade;
SELECT SQL_NO_CACHE cidade,AVG(capacidade) FROM EMPRESA AS E,LOCAL AS L WHERE L.cnpj=E.cnpj GROUP BY cidade;

SHOW profiles;
