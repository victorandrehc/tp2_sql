# -*- coding: latin1-*-
from db_manager import DatabaseManager
from models.empresa import Empresa
from models.certificado import Certificado
from models.cnae import Cnae
from models.lingua import Lingua
from models.local import Local
from models.certificado_cnae import CertificadoCnae
from models.email import Email
from models.telefone import Telefone
from models.empresa_lingua import EmpresaLingua

if __name__ == '__main__':
	db_manager=DatabaseManager()

	#Inserção de cnaes
	file =open("tabelas/CNAE.csv","r")
	file.readline()
	cnaes=[Cnae(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO CNAE VALUES "
	for e in cnaes:
		comando+=e.insert()
		if e!=cnaes[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_cnaes=db_manager.execute("SELECT * FROM CNAE;")
	#print [len(cnaes),len(teste_cnaes['tuples'])]
	if len(cnaes)==len(teste_cnaes['tuples']):
		print "Inseriu todas as cnaes: {}".format(len(cnaes))
	cnaes_recuperadas=[Cnae(list(t)) for t in teste_cnaes['tuples']]

	#Inserção de linguas
	file =open("tabelas/LINGUA.csv","r")
	file.readline()
	linguas=[Lingua(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO LINGUA VALUES "
	for e in linguas:
		comando+=e.insert()
		if e!=linguas[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_linguas=db_manager.execute("SELECT * FROM LINGUA;")
	#print [len(linguas),len(teste_linguas['tuples'])]
	if len(linguas)==len(teste_linguas['tuples']):
		print "Inseriu todas as linguas: {}".format(len(linguas))
	linguas_recuperadas=[Lingua(list(t)) for t in teste_linguas['tuples']]


	#Inserção de certificados
	file =open("tabelas/CERTIFICADO.csv","r")
	file.readline()
	certificados=[Certificado(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO CERTIFICADO VALUES "
	for e in certificados:
		comando+=e.insert()
		if e!=certificados[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_certificados=db_manager.execute("SELECT * FROM CERTIFICADO;")
	#print [len(certificados),len(teste_certificados['tuples'])]
	if len(certificados)==len(teste_certificados['tuples']):
		print "Inseriu todos os certificados: {}".format(len(certificados))
	certificados_recuperadas=[Certificado(list(t)) for t in teste_certificados['tuples']]
	
	#Inserção de empresas
	file =open("tabelas/EMPRESA.csv","r")
	file.readline()
	empresas=[Empresa(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO EMPRESA VALUES "
	for e in empresas:
		comando+=e.insert()
		if e!=empresas[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_empresas=db_manager.execute("SELECT * FROM EMPRESA;")
	#print [len(empresas),len(teste_empresas['tuples'])]
	if len(empresas)==len(teste_empresas['tuples']):
		print "Inseriu todas as empresas: {}".format(len(empresas))
	empresas_recuperadas=[Empresa(list(t)) for t in teste_empresas['tuples']]


	#Inserção de locais
	file =open("tabelas/LOCAL.csv","r")
	file.readline()
	locais=[Local(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO LOCAL VALUES "
	for e in locais:
		comando+=e.insert()
		if e!=locais[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_locais=db_manager.execute("SELECT * FROM LOCAL;")
	#print [len(locais),len(teste_locais['tuples'])]
	if len(locais)==len(teste_locais['tuples']):
		print "Inseriu todas as locais: {}".format(len(locais))
	locais_recuperadas=[Local(list(t)) for t in teste_locais['tuples']]

	#Inserção de certificado_cnaes
	file =open("tabelas/CERTIFICADO_CNAE.csv","r")
	file.readline()
	certificado_cnaes=[CertificadoCnae(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO CERTIFICADO_CNAE VALUES "
	for e in certificado_cnaes:
		comando+=e.insert()
		if e!=certificado_cnaes[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)

	
	#print comando
	teste_certificado_cnaes=db_manager.execute("SELECT * FROM CERTIFICADO_CNAE;")
	#print [len(certificado_cnaes),len(teste_certificado_cnaes['tuples'])]
	if len(certificado_cnaes)==len(teste_certificado_cnaes['tuples']):
		print "Inseriu todos os certificado_cnaes: {}".format(len(certificado_cnaes))
	certificado_cnaes_recuperadas=[CertificadoCnae(list(t)) for t in teste_certificado_cnaes['tuples']]

	#Inserção de emails
	file =open("tabelas/EMPRESA_EMAIL.csv","r")
	file.readline()
	emails=[Email(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO EMPRESA_EMAIL VALUES "
	for e in emails:
		comando+=e.insert()
		if e!=emails[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_emails=db_manager.execute("SELECT * FROM EMPRESA_EMAIL;")
	#print [len(emails),len(teste_emails['tuples'])]
	if len(emails)==len(teste_emails['tuples']):
		print "Inseriu todos os emails: {}".format(len(emails))
	emails_recuperadas=[Email(list(t)) for t in teste_emails['tuples']]

	#Inserção de telefones
	file =open("tabelas/EMPRESA_TELEFONE.csv","r")
	file.readline()
	telefones=[Telefone(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO EMPRESA_TELEFONE VALUES "
	for e in telefones:
		comando+=e.insert()
		if e!=telefones[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_telefones=db_manager.execute("SELECT * FROM EMPRESA_TELEFONE;")
	#print [len(telefones),len(teste_telefones['tuples'])]
	if len(telefones)==len(teste_telefones['tuples']):
		print "Inseriu todos os telefones: {}".format(len(telefones))
	telefones_recuperadas=[Telefone(list(t)) for t in teste_telefones['tuples']]

	#Inserção de empresa_lingua
	file =open("tabelas/EMPRESA_LINGUA.csv","r")
	file.readline()
	empresa_lingua=[EmpresaLingua(f.split(';')) for f in file]
	file.close()

	comando="INSERT INTO EMPRESA_LINGUA VALUES "
	for e in empresa_lingua:
		comando+=e.insert()
		if e!=empresa_lingua[-1]:
			comando+=","
		else:
			comando+=";"
	db_manager.insert(comando)
	
	#print comando
	teste_empresa_lingua=db_manager.execute("SELECT * FROM EMPRESA_LINGUA;")
	#print [len(empresa_lingua),len(teste_empresa_lingua['tuples'])]
	if len(empresa_lingua)==len(teste_empresa_lingua['tuples']):
		print "Inseriu todos as empresa_lingua: {}".format(len(empresa_lingua))
	empresa_lingua_recuperadas=[EmpresaLingua(list(t)) for t in teste_empresa_lingua['tuples']]


