# -*- coding: latin1-*-

import mysql.connector
import time

class DatabaseManager(object):
	"""docstring for DatabaseManager"""
	def __init__(self):
		#super(DatabaseManager, self).__init__()
		#self.arg = arg
		self.config={}
		file=open('conf.ig','r')
		#self.config={i.split(':')[0].rstrip('\n'):i.split(':')[1].rstrip('\n') for i in file if i!=""}
		for i in file:
			if(i!=""):
				itens=i.rstrip('\n').replace(' ','').split(':')
				if itens[1]=='True' or itens[1]=='False' or itens[1]=='TRUE' or itens[1]=='FALSE':
					if itens[1]=='False' or itens[1]=='FALSE':
						itens[1]=''
					self.config[itens[0]]=bool(itens[1])
				else: 
					self.config[itens[0]]=itens[1]
		file.close()
	
		#print self.config #descomente para ver as configurações de conexão
		cnx = mysql.connector.connect(**self.config)
		cnx.close()

	def execute(self,cmd,args=""):
		cnx=mysql.connector.connect(**self.config)
		cursor=cnx.cursor()
		#print "running: "+cmd
		elapsed=-1
		try:
			t=time.time()
			retorno=cursor.execute(cmd,args)
			elapsed=round((time.time()-t)*1000,2)
			#print "query took {}s".format(elapsed)
		except mysql.connector.Error as err:
			print err.errno

		retorno=[i for i in cursor]
		cursor.close()		
		cnx.close()
		#print "returning:\n {}".format(cursor) 
		return {'tuples':retorno,'elapsed':elapsed}

	def insert(self,cmd,args=""):
		cnx=mysql.connector.connect(**self.config)
		cursor=cnx.cursor()
		#print "running: "+cmd
		elapsed=-1
		
		t=time.time()
		retorno=cursor.execute(cmd,args)
		elapsed=round((time.time()-t)*1000,2)
		#print "query took {}s".format(elapsed)
		
		cnx.commit()
		cursor.close()		
		cnx.close()
		#print "returning:\n {}".format(cursor) 
		return {'tuples':retorno,'elapsed':elapsed}



		