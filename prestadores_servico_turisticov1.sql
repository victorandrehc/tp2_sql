-- MySQL dump 10.11
--
-- Host: localhost    Database: sbbd_25_anos
-- ------------------------------------------------------
-- Server version 5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS prestadores_servicos_turisticos;
CREATE DATABASE IF NOT EXISTS prestadores_servicos_turisticos;

USE prestadores_servicos_turisticos;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `EMPRESA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = latin1;
CREATE TABLE `EMPRESA` (
  `cnpj` varchar(20) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `situacao` varchar(200) NOT NULL,
  `capacidade` int(4),
  `cod_certif` varchar(50) NOT NULL,
  PRIMARY KEY  (`cnpj`),
  UNIQUE (`nome`,`cod_certif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;


--
-- Table structure for table `autor`
--

DROP TABLE IF EXISTS `LOCAL`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `LOCAL` (
  `cnpj` int(20) NOT NULL,
  `cep` int(10) NOT NULL,
  `uf` char(2) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `bairro`varchar(50) NOT NULL,
  `logradouro`varchar(50) NOT NULL,
  PRIMARY KEY  (`cnpj`),
  UNIQUE (`logradouro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `certificado`
--

DROP TABLE IF EXISTS `CERTIFICADO`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `CERTIFICADO` (
  `cod_certif` int(50) NOT NULL,
  `nat_jur` varchar(50) NOT NULL,
  `atividade` varchar(50) NOT NULL,
  `porte` varchar(50) NOT NULL,
  `data_ger` date NOT NULL,
  `data_val` date NOT NULL,
  PRIMARY KEY  (`cod_certif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `cnae`
--

DROP TABLE IF EXISTS `CNAE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `CNAE` (
  `id_cnae` int(10) NOT NULL,
  `codigo` int(10)  NOT NULL,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY  (`id_cnae`),
  UNIQUE(`codigo`,`descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `edicao`
--

DROP TABLE IF EXISTS `LINGUA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `LINGUA` (
  `id_lingua` int(3) NOT NULL,
  `nome` varchar(30) NOT NULL,
  PRIMARY KEY  (`id_lingua`),
  UNIQUE(`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `empresa_lingua`
--

DROP TABLE IF EXISTS `EMPRESA_LINGUA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `EMPRESA_LINGUA` (
  `cnpj` int(20) NOT NULL,
  `id_lingua` int(3) NOT NULL,
  PRIMARY KEY  (`cnpj`, `id_lingua`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `empresa_telefone`
--

DROP TABLE IF EXISTS `EMPRESA_TELEFONE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `EMPRESA_TELEFONE` (
  `telefone` int(11) NOT NULL,
  `cnpj` int(20) NOT NULL,
  PRIMARY KEY  (`telefone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `empresa_email`
--

DROP TABLE IF EXISTS `EMPRESA_EMAIL`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `EMPRESA_EMAIL` (
  `email` varchar(50) NOT NULL,
  `cnpj` int(20) NOT NULL,
  PRIMARY KEY  (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `certificado_cnae`
--

DROP TABLE IF EXISTS `CERTIFICADO_CNAE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `CERTIFICADO_CNAE` (
  `cod_certif` int(50) NOT NULL,
  `id_cnae` int(10) NOT NULL,
  PRIMARY KEY  (`cod_certif`,`id_cnae`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;