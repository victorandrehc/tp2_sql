# -*- coding: latin1 -*-
from formatos import *

class Local(object):
	"""docstring for Local"""
	def __init__(self, arg=None):
		#super(Local, self).__init__()
		#CNPJ;CEP;UF;CIDADE;BAIRRO;LOGRADOURO
		self.POSICAO_CNPJ=0
		self.POSICAO_CEP=1
		self.POSICAO_UF=2
		self.POSICAO_CIDADE=3
		self.POSICAO_BAIRRO=4
		self.POSICAO_LOGRADOURO=5

		if arg!=None:
			self.cnpj=formata_cnpj(arg[self.POSICAO_CNPJ])
			self.cep=arg[self.POSICAO_CEP]
			self.uf=arg[self.POSICAO_UF]
			self.cidade=arg[self.POSICAO_CIDADE]
			self.bairro=arg[self.POSICAO_BAIRRO]
			self.logradouro=arg[self.POSICAO_LOGRADOURO]
		else:
			self.cnpj=None
			self.cep=None
			self.uf=None
			self.cidade=None
			self.bairro=None
			self.logradouro=None

	def insert(self):
		'''
		SET character_set_client = latin1;
	CREATE TABLE `LOCAL` (
	  `cnpj` varchar(18) NOT NULL,
	  `cep` int(8) NOT NULL,
	  `uf` char(2) NOT NULL,
	  `cidade` varchar(40) NOT NULL,
	  `bairro`varchar(48) NOT NULL,
	  `logradouro`varchar(89) NOT NULL,
	  PRIMARY KEY  (`cnpj`),
	  FOREIGN KEY(`cnpj`) REFERENCES `EMPRESA`(`cnpj`) ON DELETE CASCADE,
	  UNIQUE (`logradouro`)
			'''
		return "('"+self.cnpj+"','"+self.cep+"','"+self.uf+"','"+self.cidade+"','"+self.bairro+"','"+self.logradouro+"')"

if __name__ == '__main__':
	file=open("../tabelas/LOCAL.csv","r")
	file.readline()
	locais=[Local(f.split(';')) for f in file]
	file.close()
	for l in locais:
		print l
	print "foram obtidas: {} locais".format(len(locais))
	comando="INSERT INTO `LOCAL` VALUES "+locais[0].insert()
	for l in locais[1:5]:
		comando+=","+l.insert()
	comando+=";"
	print comando	
