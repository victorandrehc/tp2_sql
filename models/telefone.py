# -*- coding: latin1 -*-
from formatos import *

class Telefone(object):
	"""docstring for Telefone"""
	def __init__(self, arg=None):
		#CNPJ;TELEFONE;;;
		self.POSICAO_CNPJ=0
		self.POSICAO_TELEFONE=1

		if arg!=None:
			self.cnpj=formata_cnpj(arg[self.POSICAO_CNPJ])
			self.telefone=arg[self.POSICAO_TELEFONE]
		else:
			self.cnpj=None
			self.telefone=None

	def insert(self):
		'''
		CREATE TABLE `EMPRESA_TELEFONE` (
  		`telefone` varchar(15) NOT NULL,
  		`cnpj` varchar(18) NOT NULL,
  		PRIMARY KEY  (`telefone`,`cnpj`),
		'''
		return "('"+self.telefone+"','"+self.cnpj+"')"

if __name__ == '__main__':
	file=open("../tabelas/EMPRESA_TELEFONE.csv","r")
	file.readline()
	telefones=[Telefone(f.split(';')) for f in file]
	file.close()
	for e in telefones:
		print e
	print "foram obtidas: {} telefones".format(len(telefones))
	comando="INSERT INTO `EMPRESA_TELEFONE` VALUES "+telefones[0].insert()
	for e in telefones[1:5]:
		comando+=","+e.insert()
	comando+=";"
	print comando	
