# -*- coding: latin1 -*-
from formatos import *
class Certificado(object):
	"""docstring for Certificado"""
	def __init__(self, arg=None):
		#CERTIF_ID;COD_CERTIF;NAT_JUR;ATIVIDADE;PORTE;DATA_GER;DATA_VAL
		self.POSICAO_ID=0
		self.POSICAO_COD_CERTIFICADO=1
		self.POSICAO_NATUREZA_JURIDICA=2
		self.POSICAO_ATIVIDADE=3
		self.POSICAO_PORTE=4
		self.POSICAO_DATA_GERACAO=5
		self.POSICAO_DATA_VALIDADE=6

		if arg!=None:
			self.id=formata_id(arg[self.POSICAO_ID])
			self.cod_certificado=formata_cod_certificado(arg[self.POSICAO_COD_CERTIFICADO])
			self.natureza_juridica=arg[self.POSICAO_NATUREZA_JURIDICA]
			self.atividade=arg[self.POSICAO_ATIVIDADE]
			self.porte=arg[self.POSICAO_PORTE]
			self.data_geracao=arg[self.POSICAO_DATA_GERACAO]
			self.data_validade=arg[self.POSICAO_DATA_VALIDADE]
		else:
			self.id=None
			self.cod_certificado=None
			self.natureza_juridica=None
			self.atividade=None
			self.porte=None
			self.data_geracao=None
			self.data_validade=None

	def insert(self):
		'''
		CREATE TABLE `CERTIFICADO` (
  		`id_certif` int(3),
  		`cod_certif` varchar(19) NOT NULL,
  		`nat_jur` varchar(34) NOT NULL,
  		`atividade` varchar(11) NOT NULL,
  		`porte` varchar(32) NOT NULL,
  		`data_ger` varchar(10) NOT NULL,
  		`data_val` varchar(10) NOT NULL,
  		'''
		return "("+str(self.id)+",'"+self.cod_certificado+"','"+self.natureza_juridica+"','"+self.atividade+"','"+self.porte+"','"+self.data_geracao+"','"+self.data_validade+"')"

if __name__ == '__main__':
	file=open("../tabelas/CERTIFICADO.csv","r")
	file.readline()
	certificados=[Certificado(f.split(';')) for f in file]
	file.close()
	for e in certificados:
		print e
	print "foram obtidas: {} certificados".format(len(certificados))
	comando="INSERT INTO `CERTIFICADO` VALUES "+certificados[0].insert()
	for e in certificados[1:5]:
		comando+=","+e.insert()
	comando+=";"
	print comando	
