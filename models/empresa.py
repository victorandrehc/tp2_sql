# -*- coding: latin1 -*-
from formatos import *
class Empresa(object):
	"""docstring for Empresa"""
	def __init__(self, arg=None):
		#CNPJ;RAZAO_SOCIAL;NOME_FANTASIA;SITUACAO;CAPACIDADE;ID_CERTIF;;;
		self.POSICAO_CNPJ=0
		self.POSICAO_RAZAO_SOCIAL=1
		self.POSICAO_NOME=2
		self.POSICAO_SITUACAO=3
		self.POSICAO_CAPACIDADE=4
		self.POSICAO_ID_CERTIFICADO=5

		if arg != None:
			#self.cnpj=int(arg[self.POSICAO_CNPJ].replace(".","").replace("/","").replace("-",""))
			self.cnpj=formata_cnpj(arg[self.POSICAO_CNPJ])
			self.razao_social=arg[self.POSICAO_RAZAO_SOCIAL]
			self.nome=arg[self.POSICAO_NOME]
			self.situacao=arg[self.POSICAO_SITUACAO]
			self.capacidade=int(arg[self.POSICAO_CAPACIDADE])
			#self.cod_certificado=int(arg[self.POSICAO_COD_CERTIFICADO].replace(".","").replace("-",""))
			self.id_certificado=formata_id(arg[self.POSICAO_ID_CERTIFICADO])
		else:
			self.cnpj=None
			self.razao_social=None
			self.nome=None
			self.situacao=None
			self.capacidade=None
			self.id_certificado=None


	def insert(self):
		'''
		CREATE TABLE `EMPRESA` (
		  `cnpj` varchar(18) NOT NULL,
		  `razao_social` varchar(69) NOT NULL,
		  `nome_fant` varchar(69),
		  `situacao` varchar(14) NOT NULL,
		  `capacidade` int(4),
		  `id_certif` int(3) NOT NULL,
		'''
		return "('"+str(self.cnpj)+"','"+self.razao_social+"','"+self.nome+"','"+self.situacao+"',"+str(self.capacidade)+",'"+str(self.id_certificado)+"')"


if __name__ == '__main__':
	file=open("../tabelas/EMPRESA.csv","r")
	file.readline()
	empresas=[Empresa(f.split(';')) for f in file]
	file.close()
	for e in empresas:
		print e
	print "foram obtidas: {} empresas".format(len(empresas))
	comando="INSERT INTO `EMPRESA` VALUES "+empresas[0].insert()
	for e in empresas[1:5]:
		comando+=","+e.insert()
	comando+=";"
	print comando	

