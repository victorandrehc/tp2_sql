# -*- coding: latin1-*-
from formatos import *
class ResultadoPesquisaBasica(object):
	"""docstring for ResultadoPesquisaBasica"""
	def __init__(self, arg=None):
		self.HEADER=['cnpj','razao social','nome fantasia','logradouro','cidade']

		self.POSICAO_CNPJ=0
		self.POSICAO_RAZAO_SOCIAL=1
		self.POSICAO_NOME_FANTASIA=2
		self.POSICAO_LOGRADOURO=3
		self.POSICAO_CIDADE=4

		if arg!=None:
			self.cnpj=formata_cnpj(arg[self.POSICAO_CNPJ])
			self.razao_social=arg[self.POSICAO_RAZAO_SOCIAL]
			self.nome_fantasia=arg[self.POSICAO_NOME_FANTASIA]
			self.logradouro=arg[self.POSICAO_LOGRADOURO]
			self.cidade=arg[self.POSICAO_CIDADE]
		else:
			self.cnpj=None
			self.razao_social=None
			self.nome_fantasia=None
			self.logradouro=None
			self.cidade=None

	def get_resultado(self):
		return [self.cnpj,self.razao_social,self.nome_fantasia,self.logradouro,self.cidade]

class ResultadoPesquisaAvancada(object):
	"""docstring for ResultadoConsultaAVANCADA"""
	def __init__(self, arg=None):
		self.HEADER=['Grupo','Resultado Agregado']
		self.POSICAO_GRUPO=0
		self.POSICAO_RESULTADO_AGREGADO=1
		if arg!=None:
			self.grupo=arg[self.POSICAO_GRUPO]
			self.resultado_agregado=str(arg[self.POSICAO_RESULTADO_AGREGADO])
		else:
			self.grupo=None
			self.resultado_agregado=None

	def get_resultado(self):
		return [self.grupo,self.resultado_agregado]
		