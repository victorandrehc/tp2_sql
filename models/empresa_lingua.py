# -*- coding: latin1 -*-
from formatos import *

class EmpresaLingua(object):
	"""docstring for EmpresaLingua"""
	def __init__(self, arg=None):
		#CNPJ;ID_LINGUA
		self.POSICAO_CNPJ=0
		self.POSICAO_ID_LINGUA=1

		if arg!=None:
			self.cnpj=formata_cnpj(arg[self.POSICAO_CNPJ])
			self.id_lingua=formata_id(arg[self.POSICAO_ID_LINGUA])
		else:
			self.cnpj=None
			self.id_lingua=None

	def insert(self):
		'''
		CREATE TABLE `EMPRESA_LINGUA` (
  		`cnpj` varchar(18) NOT NULL,
  		`id_lingua` int(2) NOT NULL,
		'''
		return "('"+self.cnpj+"',"+str(self.id_lingua)+")"

if __name__ == '__main__':
	file=open("../tabelas/EMPRESA_LINGUA.csv","r")
	file.readline()
	linguas=[EmpresaLingua(f.split(';')) for f in file]
	file.close()
	for e in linguas:
		print e
	print "foram obtidas: {} linguas".format(len(linguas))
	comando="INSERT INTO `EMPRESA_LINGUA` VALUES "+linguas[0].insert()
	for e in linguas[1:5]:
		comando+=","+e.insert()
	comando+=";"
	print comando
