# -*- coding: latin1 -*-
from formatos import *
class Lingua(object):
	"""docstring for Lingua"""
	def __init__(self, arg=None):
		#CODIGO;NOME
		self.POSICAO_ID=0
		self.POSICAO_NOME=1
		if arg!=None:
			self.id=formata_id(arg[self.POSICAO_ID])
			self.nome=arg[self.POSICAO_NOME]
		else:
			self.id=None
			self.nome=None

	def insert(self):
		'''
		CREATE TABLE `LINGUA` (
  		`id_lingua` int(2) NOT NULL,
  		`nome` varchar(8) NOT NULL,
  		PRIMARY KEY  (`id_lingua`),
		'''
		return "("+str(self.id)+",'"+self.nome+"')"

if __name__ == '__main__':
	file=open("../tabelas/LINGUA.csv","r")
	file.readline()
	linguas=[Lingua(f.split(';')) for f in file]
	file.close()
	for e in linguas:
		print e
	print "foram obtidas: {} linguas".format(len(linguas))
	comando="INSERT INTO `LINGUA` VALUES "+linguas[0].insert()
	for e in linguas[1:5]:
		comando+=","+e.insert()
	comando+=";"
	print comando
