# -*- coding: latin1 -*-
from formatos import *

class CertificadoCnae(object):
	"""docstring for CertificadoCnae"""
	def __init__(self, arg=None):
		#CERTIF_ID;CNAE_ID
		self.POSICAO_CERTIFICADO_ID=0
		self.POSICAO_CNAE_ID=1

		if arg!=None:
			self.certificado_id=formata_id(arg[self.POSICAO_CERTIFICADO_ID])
			self.cnae_id=formata_id(arg[self.POSICAO_CNAE_ID])
		else:
			self.certificado_id=None
			self.cnae_id=None

	def insert(self):
		'''
		CREATE TABLE `CERTIFICADO_CNAE` (
	  `id_certif` int(3),
	  `id_cnae` int(2) NOT NULL,
		'''
		return "("+str(self.certificado_id)+","+str(self.cnae_id)+")"

if __name__ == '__main__':
	file=open("../tabelas/CERTIFICADO_CNAE.csv","r")
	file.readline()
	certificado_cnaes=[CertificadoCnae(f.split(';')) for f in file]
	file.close()
	for e in certificado_cnaes:
		print e
	print "foram obtidas: {} certificado_cnaes".format(len(certificado_cnaes))
	comando="INSERT INTO `CERTIFICADO_CNAE` VALUES "+certificado_cnaes[0].insert()
	for e in certificado_cnaes[1:5]:
		comando+=","+e.insert()
	comando+=";"
	print comando	

