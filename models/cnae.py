# -*- coding: latin1 -*-
from formatos import *
class Cnae(object):
	"""docstring for Cnae"""
	def __init__(self, arg=None):
		#CNAE_ID;COD;DESCRICAO
		self.POSICAO_ID=0
		self.POSICAO_CODIGO=1
		self.POSICAO_DESCRICAO=2
		if arg!=None:
			self.id=formata_id(arg[self.POSICAO_ID])
			self.codigo=arg[self.POSICAO_CODIGO]
			self.descricao=arg[self.POSICAO_DESCRICAO]
		else:
			self.id=None
			self.codigo=None
			self.descricao=None

	def insert(self):
		'''
			CREATE TABLE `CNAE` (
		  `id_cnae` int(2) NOT NULL,
		  `codigo` varchar(10)  NOT NULL,
		  `descricao` varchar(117) NOT NULL,
		  PRIMARY KEY  (`id_cnae`),
		  UNIQUE(`codigo`,`descricao`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		'''
		return "("+str(self.id)+",'"+self.codigo+"','"+self.descricao+"')"

if __name__ == '__main__':
	file=open("../tabelas/CNAE.csv","r")
	file.readline()
	cnaes=[Cnae(f.split(';')) for f in file]
	file.close()
	for e in cnaes:
		print e
	print "foram obtidas: {} cnaes".format(len(cnaes))
	comando="INSERT INTO `CNAE` VALUES "+cnaes[0].insert()
	for e in cnaes[1:5]:
		comando+=","+e.insert()
	comando+=";"
	print comando
